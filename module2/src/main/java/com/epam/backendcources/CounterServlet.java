package com.epam.backendcources;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/*For Testing: for (let i=0; i<100; i++) {fetch("count");}*/
public class CounterServlet extends HttpServlet {
    private static int n = 0;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        incrementAndPrint();
        out.print("<h1>Hello Servlet</h1>");
    }

    private synchronized void incrementAndPrint() {
        System.out.println(++n);
    }

}
