package com.epam.backendcources;

import javax.servlet.*;
import java.io.IOException;

public class Filter1 implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("Before 1");
        chain.doFilter(request, response);
        System.out.println("After 1");
    }

    public void destroy() {

    }
}
