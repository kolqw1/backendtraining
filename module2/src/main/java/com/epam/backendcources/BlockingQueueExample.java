package com.epam.backendcources;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BlockingQueueExample {
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue queue = new ArrayBlockingQueue(4);
        new Thread(() -> {
            int i = 0;
            while (true) {
                queue.add(i++);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(() -> {
            int i = 1000;
            while (true) {
                queue.add(i++);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(() -> {
            while (true) {
                try {
                    System.out.println("Size: " + queue.size());
                    System.out.println(queue.poll());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        Thread.currentThread().join();
    }
}
