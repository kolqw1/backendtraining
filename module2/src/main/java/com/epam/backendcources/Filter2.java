package com.epam.backendcources;

import javax.servlet.*;
import java.io.IOException;

public class Filter2 implements Filter {
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("Before 2");
        chain.doFilter(request, response);
        System.out.println("After 2");
    }

    public void destroy() {

    }
}
