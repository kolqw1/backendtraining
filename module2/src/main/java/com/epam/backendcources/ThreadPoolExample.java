package com.epam.backendcources;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolExample {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(2);
        executor.submit(() -> {
            while (true) {
                System.out.println(1);
                Thread.sleep(500);
            }
        });
        executor.submit(() -> {
            for (int i = 0; i < 10; i++) {
                System.out.println(2);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        executor.submit(() -> {
            while (true) {
                System.out.println(3);
                Thread.sleep(500);
            }
        });
    }
}
