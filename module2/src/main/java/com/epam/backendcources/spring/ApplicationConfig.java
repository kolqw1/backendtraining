package com.epam.backendcources.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Bean
    public static C1 c1() {
        return new C1();
    }

    @Bean
    public static C2 c2(C1 c1) {
        return new C2().setC1(c1);
    }

}
