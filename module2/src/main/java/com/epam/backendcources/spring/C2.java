package com.epam.backendcources.spring;

public class C2 {
    //@Autowired
    private C1 c1;

    public String c1() {
        return c1.c1();
    }

    public C2 setC1(C1 c1) {
        this.c1 = c1;
        return this;
    }
}
