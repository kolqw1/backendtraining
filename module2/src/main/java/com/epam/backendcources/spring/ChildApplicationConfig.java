package com.epam.backendcources.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChildApplicationConfig {

    @Bean
    public static C3 c3(C2 c2) {
        return new C3().setC2(c2);
    }

}
