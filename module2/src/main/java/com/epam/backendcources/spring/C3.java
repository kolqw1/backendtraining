package com.epam.backendcources.spring;

import org.springframework.beans.factory.annotation.Autowired;

public class C3 {
    private C2 c2;

    public String c1() {
        return c2.c1();
    }

    @Autowired
    public C3 setC2(C2 c2) {
        this.c2 = c2;
        return this;
    }
}
