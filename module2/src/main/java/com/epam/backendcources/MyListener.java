package com.epam.backendcources;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

public class MyListener implements ServletRequestListener {

    public void requestDestroyed(ServletRequestEvent sre) {
        System.out.print("Destroyed ");
        printParams(sre);
    }

    public void requestInitialized(ServletRequestEvent sre) {
        System.out.print("Initialized ");
        printParams(sre);
    }

    private void printParams(ServletRequestEvent sre) {
        sre.getServletRequest().getParameterMap().forEach((k, v) -> System.out.println(k + "=" + v[0]));
    }
}
