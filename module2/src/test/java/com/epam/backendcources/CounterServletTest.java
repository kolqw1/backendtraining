package com.epam.backendcources;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CounterServletTest {

    /*1, 2, 4, 8, 100, 1000 threads: 30, 20, 15, 15, 15, 15 (18 first time) seconds*/
    @Test
    public void testDoGet() throws InterruptedException {
        RestTemplate restTemplate = new RestTemplate();
        int poolSize = 10;
        ExecutorService executor = Executors.newFixedThreadPool(poolSize);
        List<Callable<ResponseEntity>> tasks = new ArrayList<>(poolSize);
        for (int i = 0; i < poolSize; i++) {
            tasks.add(() -> {
                try {
                    send();
                    //return restTemplate.getForEntity("http://localhost:8080/count", Void.class);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                return null;
            });
        }
        long t = System.currentTimeMillis();
        executor.invokeAll(tasks);
        System.out.println(System.currentTimeMillis() - t);
    }

    /*maxConnections-number of requests, acceptCount - number of connections*/
    private void send() throws IOException {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(10);
        HttpClient client = HttpClientBuilder.create().setConnectionManager(connectionManager).build();
        try {
            client.execute(new HttpGet("http://localhost:8080/count"));
        } catch (Exception e) {
            System.out.println("1 " + e.getMessage());
        }
        try {
            client.execute(new HttpGet("http://localhost:8080/count"));
        } catch (Exception e) {
            System.out.println("2 " + e.getMessage());
        }
    }
}