package com.epam.backendcources.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@ContextConfiguration(classes = ApplicationConfig.class)
public class SpringJavaTest extends AbstractTestNGSpringContextTests {
    @Autowired
    C2 c2;

    @Test
    public void testC1() throws Exception {
        assertEquals("c1", c2.c1());
    }

}