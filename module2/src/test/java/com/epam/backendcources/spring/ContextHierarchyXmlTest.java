package com.epam.backendcources.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@ContextConfiguration({"classpath:applicationContext.xml"})
public class ContextHierarchyXmlTest extends AbstractTestNGSpringContextTests {
    @Autowired
    ApplicationContext parent;

    @Test
    public void test() throws Exception {
        ApplicationContext child = new ClassPathXmlApplicationContext(new String[]{"childApplicationContext.xml"}, parent);

        C2 c2 = child.getBean(C2.class);
        assertEquals("c1", c2.c1());

        C3 c3 = child.getBean(C3.class);
        assertEquals("c1", c3.c1());
    }

}