package com.epam.backendcources;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class LogAspect {
    @Before("(@annotation(CustomLog) || @within(CustomLog)) && execution(public !static * *(..))")
    public void log(JoinPoint joinPoint) {
        System.out.println("Before " + joinPoint.getSignature());
    }
}
