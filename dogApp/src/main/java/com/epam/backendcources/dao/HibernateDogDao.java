package com.epam.backendcources.dao;

import com.epam.backendcources.Dog;
import lombok.AllArgsConstructor;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;
import java.sql.Date;

@AllArgsConstructor
public class HibernateDogDao implements DogDao {
    private final SessionFactory sessionFactory;

    @Override
    public Dog create(Dog dog) {
        sessionFactory.getCurrentSession().save(dog);
        return dog;
    }

    @Override
    public Dog get(int id) {
        return sessionFactory.getCurrentSession().get(Dog.class, id);
    }

    @Override
    public Dog update(Dog dog) {
        EntityManager session = sessionFactory.getCurrentSession();
        int result = session.createQuery("update Dog set name=?1, birth_date=?2, height=?3, weight=?4 where id=?5")
                .setParameter(1, dog.getName())
                .setParameter(2, Date.valueOf(dog.getBirthDate()))
                .setParameter(3, dog.getHeight())
                .setParameter(4, dog.getWeight())
                .setParameter(5, dog.getId())
                .executeUpdate();
        return result != 0 ? dog : null;
    }

    @Override
    public boolean delete(int id) {
        EntityManager session = sessionFactory.getCurrentSession();
        return session.createQuery("delete from Dog where id=:id")
                .setParameter("id", id).executeUpdate() != 0;
    }
}
