package com.epam.backendcources.dao;

import com.epam.backendcources.Dog;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class InMemoryDogDao implements DogDao {
    private Map<Integer, Dog> dogs = new ConcurrentHashMap<>();
    private AtomicInteger counter = new AtomicInteger(0);

    public Dog create(Dog dog) {
        dog.setId(counter.incrementAndGet());
        dogs.put(dog.getId(), dog);
        return dog;
    }

    public Dog get(int id) {
        return dogs.get(id);
    }

    public Dog update(Dog dog) {
        if (dogs.get(dog.getId()) == null) {
            return null;
        }
        dogs.put(dog.getId(), dog);
        return dog;
    }

    public boolean delete(int id) {
        return dogs.remove(id) != null;
    }
}
