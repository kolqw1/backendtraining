package com.epam.backendcources.dao;

import com.epam.backendcources.Dog;

public interface DogDao {
    Dog create(Dog dog);

    Dog get(int id);

    Dog update(Dog dog);

    boolean delete(int id);
}
