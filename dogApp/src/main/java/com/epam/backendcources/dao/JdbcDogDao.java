package com.epam.backendcources.dao;

import com.epam.backendcources.Dog;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

@AllArgsConstructor
public class JdbcDogDao implements DogDao {
    private final JdbcTemplate jdbcTemplate;
    private final DogMapper dogMapper = new DogMapper();

    public Dog create(Dog dog) {
        int sequenceValue = jdbcTemplate.queryForObject("select DOG_SEQ.NEXTVAL from dual", Integer.class);
        dog.setId(sequenceValue);
        int result = jdbcTemplate.update("insert into dog (id, name, birth_date, height, weight) values (?, ?, ?, ?, ?)",
                dog.getId(), dog.getName(), Date.valueOf(dog.getBirthDate()), dog.getHeight(), dog.getWeight());
        return result != 0 ? dog : null;
    }

    public Dog get(int id) {
        try {
            return jdbcTemplate.queryForObject("select * from dog where id=?", dogMapper, id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public Dog update(Dog dog) {
        int result = jdbcTemplate.update("update dog set name=?, birth_date=?, height=?, weight=? where id=?",
                dog.getName(), Date.valueOf(dog.getBirthDate()), dog.getHeight(), dog.getWeight(), dog.getId());
        return result != 0 ? dog : null;
    }

    public boolean delete(int id) {
        return jdbcTemplate.update("delete from dog where id=?", id) != 0;
    }

    private class DogMapper implements RowMapper<Dog> {
        @Override
        public Dog mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            Dog dog = new Dog();
            dog.setId(resultSet.getInt("id"));
            dog.setName(resultSet.getString("name"));
            dog.setBirthDate(resultSet.getDate("birth_date").toLocalDate());
            dog.setHeight(resultSet.getInt("height"));
            dog.setWeight(resultSet.getInt("weight"));
            return dog;
        }
    }
}
