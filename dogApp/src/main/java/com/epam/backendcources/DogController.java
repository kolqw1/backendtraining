package com.epam.backendcources;

import com.epam.backendcources.service.DogService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@AllArgsConstructor
@CustomLog
public class DogController {
    public static final String DOG = "/dog";
    public static final String DOG_ID = DOG + "/{id}";

    private final DogService dogService;

    @PostMapping(value = DOG, produces = MediaType.APPLICATION_JSON_VALUE)
    public Dog create(@Valid @RequestBody Dog dog) {
        return dogService.create(dog);
    }

    @GetMapping(DOG_ID)
    public Dog get(@PathVariable int id) {
        Dog dog = dogService.get(id);
        if (dog == null) {
            throw new NotFoundException(id);
        }
        return dog;
    }

    @PutMapping(value = DOG_ID, produces = MediaType.APPLICATION_JSON_VALUE)
    public Dog update(@PathVariable int id, @Valid @RequestBody Dog dog) {
        dog.setId(id);
        dog = dogService.update(dog);
        if (dog == null) {
            throw new NotFoundException(id);
        }
        return dog;
    }

    @DeleteMapping(DOG_ID)
    public void delete(@PathVariable int id) {
        if (!dogService.delete(id)) {
            throw new NotFoundException(id);
        }
    }

}
