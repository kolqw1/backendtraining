package com.epam.backendcources;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
public class Dog {
    private int id;
    @Size(min = 1, max = 100)
    private String name;
    @NotNull
    @PastOrPresent
    private LocalDate birthDate;
    @Positive
    private int height;
    @Positive
    private int weight;

    private Set<Toy> toys = new HashSet<>();

    public void setName(String name) {
        this.name = name.trim();
    }

    public static Dog validDog() {
        Dog dog = new Dog();
        dog.setName(RandomStringUtils.randomAlphanumeric(10));
        dog.setBirthDate(LocalDate.of(RandomUtils.nextInt(1999, 2019), 1, 1));
        dog.setHeight(RandomUtils.nextInt(1, 100));
        dog.setWeight(RandomUtils.nextInt(1, 100));
        return dog;
    }
}
