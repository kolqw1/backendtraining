package com.epam.backendcources.service;

import com.epam.backendcources.CustomLog;
import com.epam.backendcources.Dog;
import com.epam.backendcources.dao.DogDao;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;


@AllArgsConstructor
@Transactional
public class DogService {
    private final DogDao dogDao;

    @CustomLog
    public Dog create(Dog dog) {
        return dogDao.create(dog);
    }

    public Dog get(int id) {
        Dog dog = dogDao.get(id);
        if (dog != null) {
            dog.getToys().size();
        }
        return dog;
    }

    public Dog update(Dog dog) {
        return dogDao.update(dog);
    }

    public boolean delete(int id) {
        return dogDao.delete(id);
    }
}
