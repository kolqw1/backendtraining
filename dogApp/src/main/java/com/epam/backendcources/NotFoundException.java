package com.epam.backendcources;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class NotFoundException extends ResponseStatusException {
    public NotFoundException(int id) {
        super(HttpStatus.NOT_FOUND, "Dog " + id + " is not found.");
    }
}
