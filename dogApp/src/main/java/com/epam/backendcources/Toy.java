package com.epam.backendcources;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Toy {
    private int id;
    private String name;
    private Dog dog;
}
