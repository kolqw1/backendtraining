package com.epam.backendcources.dao;

import com.epam.backendcources.ComponentTest;
import com.epam.backendcources.Dog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ComponentTest
@Ignore
public class DogDaoTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private DogDao dogDao;

    @Test
    public void getsAllDogFields_afterCreation() {
        Dog testDog = Dog.validDog();
        Dog createdDog = dogDao.create(testDog);
        assertReflectionEquals(testDog, createdDog);

        createdDog = dogDao.get(createdDog.getId());
        assertReflectionEquals(testDog, createdDog);
    }

    @Test
    public void getsAllDogFields_afterUpdate() {
        Dog dog = createValid();
        Dog dogToUpdate = Dog.validDog();
        dogToUpdate.setId(dog.getId());
        Dog updatedDog = dogDao.update(dogToUpdate);
        assertReflectionEquals(dogToUpdate, updatedDog);

        updatedDog = dogDao.get(dog.getId());
        assertReflectionEquals(dogToUpdate, updatedDog);
    }

    @Test
    public void getReturnsNull_afterDeletion() {
        Dog dog = createValid();
        assertTrue(dogDao.delete(dog.getId()));
        assertNull(dogDao.get(dog.getId()));
    }

    @Test
    public void getNonexistentReturnsNull() {
        assertNull(dogDao.get(-1));
    }

    @Test
    public void deleteNonexistentReturnsFalse() {
        assertFalse(dogDao.delete(-1));
    }

    private Dog createValid() {
        return dogDao.create(Dog.validDog());
    }
}