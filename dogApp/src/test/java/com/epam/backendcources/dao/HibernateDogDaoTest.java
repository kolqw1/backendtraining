package com.epam.backendcources.dao;

import com.epam.backendcources.ComponentTest;
import com.epam.backendcources.Dog;
import com.epam.backendcources.Toy;
import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.persistence.EntityManager;
import javax.validation.ConstraintViolationException;
import java.util.Iterator;
import java.util.List;

import static org.testng.Assert.*;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ComponentTest
public class HibernateDogDaoTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private HibernateDogDao dogDao;
    @Autowired
    private SessionFactory sessionFactory;

    @Test
    public void getsAllDogFields_afterCreation() {
        Dog testDog = Dog.validDog();
        Dog createdDog = dogDao.create(testDog);
        assertReflectionEquals(testDog, createdDog);
        flushAndClear();
        createdDog = dogDao.get(createdDog.getId());
        assertReflectionEquals(testDog, createdDog);
    }

    @Test
    public void getsAllDogFields_afterUpdate() {
        Dog dog = createValid();
        flushAndClear();
        Dog dogToUpdate = Dog.validDog();
        dogToUpdate.setId(dog.getId());
        Dog updatedDog = dogDao.update(dogToUpdate);
        assertReflectionEquals(dogToUpdate, updatedDog);
        flushAndClear();
        updatedDog = dogDao.get(dog.getId());
        assertReflectionEquals(dogToUpdate, updatedDog);
    }

    @Test
    public void getReturnsNull_afterDeletion() {
        Dog dog = createValid();
        flushAndClear();
        assertTrue(dogDao.delete(dog.getId()));
        flushAndClear();
        assertNull(dogDao.get(dog.getId()));
    }

    @Test
    public void getNonexistentReturnsNull() {
        assertNull(dogDao.get(-1));
    }

    @Test
    public void deleteNonexistentReturnsFalse() {
        assertFalse(dogDao.delete(-1));
    }

    @Test
    public void savesDogWithNameOf100Symbols() {
        Dog dog = Dog.validDog();
        dog.setName(RandomStringUtils.randomAlphanumeric(100));
        createAndCheck(dog);
    }

    @Test
    public void throwsException_whenSavesDogWithNameOf101Symbols() {
        Dog dog = Dog.validDog();
        dog.setName(RandomStringUtils.randomAlphanumeric(101));
        Assert.assertThrows(ConstraintViolationException.class, () -> createAndCheck(dog));
    }

    @Test
    public void throwsException_whenSavesDogWithNullBirthDate() {
        Dog dog = Dog.validDog();
        dog.setBirthDate(null);
        Assert.assertThrows(ConstraintViolationException.class, () -> createAndCheck(dog));
    }

    @Test
    public void savesDogWithSpecialSymbolsInName() {
        Dog dog = Dog.validDog();
        dog.setName("\"' blah");
        createAndCheck(dog);
    }

    @Test
    //can't check that batch insert is working
    public void testingLazyLoading() {
        Dog dog = dogDao.create(Dog.validDog());
        for (int i = 0; i < 20; i++) {
            Toy toy = new Toy();
            toy.setDog(dog);
            sessionFactory.getCurrentSession().save(toy);
            System.out.println(i);
        }
        flushAndClear();
        dog = dogDao.get(dog.getId());
        System.out.println("-----------------");
        Iterator<Toy> iterator = dog.getToys().iterator();
        iterator.next().getName();
        iterator.next().getName();
        iterator.next().getName();
        System.out.println("-----------------");
        sessionFactory.getCurrentSession().clear();
        sessionFactory.getCurrentSession().createQuery("from Dog dog join fetch dog.toys", Dog.class).getResultList();
    }

    @Test
    //can't check that batch insert is working
    public void testingBatchSelect() {
        ((EntityManager) sessionFactory.getCurrentSession()).createQuery("delete from Dog").executeUpdate();

        for (int j = 0; j < 7; j++) {
            Dog dog = dogDao.create(Dog.validDog());
            for (int i = 0; i < 5; i++) {
                Toy toy = new Toy();
                toy.setDog(dog);
                sessionFactory.getCurrentSession().save(toy);
            }
        }
        flushAndClear();

        List<Dog> dogs = sessionFactory.getCurrentSession().createQuery("from Dog dog", Dog.class).getResultList();
        for (int i = 0; i < 5; i++) {
            dogs.get(i).getToys().iterator().next();
        }
    }

    private Dog createValid() {
        return dogDao.create(Dog.validDog());
    }

    private void createAndCheck(Dog dog) {
        Dog createdDog = dogDao.create(dog);
        assertReflectionEquals(dog, createdDog);
        flushAndClear();
        createdDog = dogDao.get(createdDog.getId());
        assertReflectionEquals(dog, createdDog);
    }

    private void flushAndClear() {
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();
    }
}