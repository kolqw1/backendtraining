package com.epam.backendcources.dao;

import com.epam.backendcources.ComponentTest;
import com.epam.backendcources.Dog;
import org.apache.commons.lang3.RandomStringUtils;
import org.h2.jdbc.JdbcSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ComponentTest
@Ignore
public class JdbcSpecificDogDaoTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private JdbcDogDao dogDao;

    @Test
    public void savesDogWithNameOf100Symbols() {
        Dog dog = Dog.validDog();
        dog.setName(RandomStringUtils.randomAlphanumeric(100));
        createAndCheck(dog);
    }

    @Test
    public void throwsException_whenSavesDogWithNameOf101Symbols() {
        Dog dog = Dog.validDog();
        dog.setName(RandomStringUtils.randomAlphanumeric(101));
        Exception exception = Assert.expectThrows(RuntimeException.class, () -> createAndCheck(dog));
        Assert.assertTrue(exception.getCause() instanceof JdbcSQLException);
        Assert.assertTrue(exception.getCause().getMessage().contains("Value too long for column \"NAME VARCHAR(100)\""));
    }

    @Test
    public void throwsException_whenSavesDogWithNullBirthDate() {
        Dog dog = Dog.validDog();
        dog.setBirthDate(null);
        Assert.assertThrows(NullPointerException.class, () -> createAndCheck(dog));
    }

    @Test
    public void savesDogWithSpecialSymbolsInName() {
        Dog dog = Dog.validDog();
        dog.setName("\"' blah");
        createAndCheck(dog);
    }

    private void createAndCheck(Dog dog) {
        Dog createdDog = dogDao.create(dog);
        assertReflectionEquals(dog, createdDog);

        createdDog = dogDao.get(createdDog.getId());
        assertReflectionEquals(dog, createdDog);
    }
}