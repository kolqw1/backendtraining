package com.epam.backendcources;

import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class DogClient {
    public Response create(Dog dog) {
        return given()
                .body(dog)
                .post(DogController.DOG);
    }

    public Response get(int id) {
        return given().get(DogController.DOG_ID, id);
    }

    public Response update(int id, Dog dog) {
        return given().body(dog).put(DogController.DOG_ID, id);
    }

    public Response delete(int id) {
        return given().delete(DogController.DOG_ID, id);
    }
}
