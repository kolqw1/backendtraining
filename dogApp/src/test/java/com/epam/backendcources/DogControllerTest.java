package com.epam.backendcources;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ComponentTest
public class DogControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private DogClient client;

    @BeforeClass
    public static void initRestAssured() {
        RestAssured.baseURI = "http://localhost:8080";
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setContentType(ContentType.JSON).setAccept(ContentType.JSON)
                .build();
    }

    @Test
    public void getsAllDogFields_afterCreation() {
        Dog testDog = Dog.validDog();
        Dog createdDog = client.create(testDog).as(Dog.class);
        testDog.setId(createdDog.getId());
        assertReflectionEquals(testDog, createdDog);

        createdDog = client.get(createdDog.getId()).as(Dog.class);
        assertReflectionEquals(testDog, createdDog);
    }

    @Test
    public void getsAllDogFields_afterUpdate() {
        Dog dog = createValid();
        Dog dogToUpdate = Dog.validDog();
        dogToUpdate.setId(dog.getId());
        Dog updatedDog = client.update(dog.getId(), dogToUpdate).as(Dog.class);
        assertReflectionEquals(dogToUpdate, updatedDog);

        updatedDog = client.get(dog.getId()).as(Dog.class);
        assertReflectionEquals(dogToUpdate, updatedDog);
    }

    @Test
    public void dogIsNotFound_afterSuccessfulDeletion() {
        Dog dog = createValid();
        assertEquals(client.delete(dog.getId()).getStatusCode(), HttpStatus.OK.value());
        assertEquals(client.get(dog.getId()).getStatusCode(), HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void getNonexistentReturns404Status() {
        assertEquals(client.get(-1).getStatusCode(), HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void updateNonexistentReturns404Status() {
        Dog dog = Dog.validDog();
        assertEquals(client.update(-1, dog).getStatusCode(), HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void deleteNonexistentReturns404Status() {
        assertEquals(client.delete(-1).getStatusCode(), HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void createInvalidReturns400Status() {
        Dog dog = Dog.validDog();
        dog.setName("");
        assertEquals(client.create(dog).getStatusCode(), HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void updateInvalidReturns400Status() {
        Dog dog = createValid();
        dog.setName("");
        assertEquals(client.update(dog.getId(), dog).getStatusCode(), HttpStatus.BAD_REQUEST.value());
    }

    private Dog createValid() {
        return client.create(Dog.validDog()).as(Dog.class);
    }

}