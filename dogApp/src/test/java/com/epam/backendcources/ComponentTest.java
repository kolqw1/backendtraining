package com.epam.backendcources;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import java.lang.annotation.*;

@WebAppConfiguration
@ContextConfiguration({"classpath:applicationContext.xml", "classpath:testApplicationContext.xml"})

@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ComponentTest {

}
