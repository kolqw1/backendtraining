package com.epam.backendcources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@ComponentTest
public class MockMvcDogControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvcDogClient client;

    @Test
    public void getsAllDogFields_afterCreation() {
        Dog testDog = Dog.validDog();
        Dog createdDog = client.getResponseAsDog(client.create(testDog));
        testDog.setId(createdDog.getId());
        assertReflectionEquals(testDog, createdDog);

        createdDog = client.getResponseAsDog(client.get(createdDog.getId()));
        assertReflectionEquals(testDog, createdDog);
    }

    @Test
    public void getsAllDogFields_afterUpdate() {
        Dog dog = createValid();
        Dog dogToUpdate = Dog.validDog();
        dogToUpdate.setId(dog.getId());
        Dog updatedDog = client.getResponseAsDog(client.update(dog.getId(), dogToUpdate));
        assertReflectionEquals(dogToUpdate, updatedDog);

        updatedDog = client.getResponseAsDog(client.get(dog.getId()));
        assertReflectionEquals(dogToUpdate, updatedDog);
    }

    @Test
    public void dogIsNotFound_afterSuccessfulDeletion() {
        Dog dog = createValid();
        assertEquals(client.delete(dog.getId()).getStatus(), HttpStatus.OK.value());
        assertEquals(client.get(dog.getId()).getStatus(), HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void getNonexistentReturns404Status() {
        assertNotFoundException(client.get(-1), "Dog -1 is not found.");
    }

    @Test
    public void updateNonexistentReturns404Status() {
        Dog dog = Dog.validDog();
        assertNotFoundException(client.update(-1, dog), "Dog -1 is not found.");
    }

    @Test
    public void deleteNonexistentReturns404Status() {
        assertNotFoundException(client.delete(-1), "Dog -1 is not found.");
    }

    @Test
    public void createInvalidReturns400Status() {
        Dog dog = Dog.validDog();
        dog.setName("");
        assertEquals(client.create(dog).getStatus(), HttpStatus.BAD_REQUEST.value());
    }

    @Test
    public void updateInvalidReturns400Status() {
        Dog dog = createValid();
        dog.setName("");
        assertEquals(client.update(dog.getId(), dog).getStatus(), HttpStatus.BAD_REQUEST.value());
    }

    private void assertNotFoundException(MockHttpServletResponse response, String errorMessage) {
        assertEquals(response.getStatus(), HttpStatus.NOT_FOUND.value());
        assertEquals(response.getErrorMessage(), errorMessage);
    }
    private Dog createValid() {
        return client.getResponseAsDog(client.create(Dog.validDog()));
    }

}