package com.epam.backendcources;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.time.LocalDate;
import java.util.Set;

import static org.testng.Assert.assertEquals;

public class DogTest {

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Test
    public void emptyAndBigNameFailsValidation() {
        Dog dog = Dog.validDog();
        dog.setName("");
        assertValidationFails(dog, "size must be between 1 and 100");
        dog.setName(RandomStringUtils.randomAlphanumeric(101));
        assertValidationFails(dog, "size must be between 1 and 100");
    }

    @Test
    public void nullAndFutureBirthDateFailsValidation() {
        Dog dog = Dog.validDog();
        dog.setBirthDate(LocalDate.now());
        assertValidationPasses(dog);
        dog.setBirthDate(LocalDate.now().plusDays(1));
        assertValidationFails(dog, "must be a date in the past or in the present");
        dog.setBirthDate(null);
        assertValidationFails(dog, "must not be null");
    }

    @Test
    public void negativeHeightFailsValidation() {
        Dog dog = Dog.validDog();
        dog.setHeight(-1);
        assertValidationFails(dog, "must be greater than 0");
    }

    @Test
    public void zeroWeightFailsValidation() {
        Dog dog = Dog.validDog();
        dog.setWeight(0);
        assertValidationFails(dog, "must be greater than 0");
    }

    @Test
    public void nameIsTrimmedInSetter() {
        Dog dog = Dog.validDog();
        String name = RandomStringUtils.randomAlphanumeric(100);
        dog.setName(name + " ");
        assertEquals(dog.getName(), name);
        assertValidationPasses(dog);
    }

    private void assertValidationPasses(Dog dog) {
        Set<ConstraintViolation<Dog>> errors = validator.validate(dog);
        assertEquals(errors.size(), 0);
    }

    private void assertValidationFails(Dog dog, String message) {
        Set<ConstraintViolation<Dog>> errors = validator.validate(dog);
        assertEquals(errors.size(), 1);
        assertEquals(errors.iterator().next().getMessage(), message);
    }

}