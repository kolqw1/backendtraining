package com.epam.backendcources;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

public class MockMvcDogClient {
    private final ObjectMapper objectMapper;
    private final MockMvc mockMvc;

    public MockMvcDogClient(ObjectMapper objectMapper, WebApplicationContext context) {
        this.objectMapper = objectMapper;
        this.mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    public MockHttpServletResponse create(Dog dog) {
        try {
            return mockMvc.perform(post(DogController.DOG)
                    .content(objectMapper.writeValueAsBytes(dog))
                    .contentType(MediaType.APPLICATION_JSON)
                    .characterEncoding("UTF-8"))
                    .andReturn().getResponse();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public MockHttpServletResponse get(int id) {
        try {
            return mockMvc.perform(MockMvcRequestBuilders.get(DogController.DOG_ID, id))
                    .andReturn().getResponse();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public MockHttpServletResponse update(int id, Dog dog) {
        try {
            return mockMvc.perform(put(DogController.DOG_ID, id)
                    .content(objectMapper.writeValueAsBytes(dog))
                    .contentType(MediaType.APPLICATION_JSON)
                    .characterEncoding("UTF-8"))
                    .andReturn().getResponse();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public MockHttpServletResponse delete(int id) {
        try {
            return mockMvc.perform(MockMvcRequestBuilders.delete(DogController.DOG_ID, id))
                    .andReturn().getResponse();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Dog getResponseAsDog(MockHttpServletResponse response) {
        try {
            return objectMapper.readValue(response.getContentAsByteArray(), Dog.class);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
